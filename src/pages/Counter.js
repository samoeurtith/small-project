import React, { Component } from 'react'

export class Counter extends Component {
    constructor(props){
        super(props);
        this.state={
            counter:0,
        }
    }
    
  render() {
    return (
      <div>
            <h2 className='text-center'>{this.state.counter}</h2>
            <div className='text-center'>
              <button className='btn btn-warning'>Increase</button>
              <button className="btn btn-danger">Decrease</button>
            </div>
      </div>
    )
  }
}

export default Counter